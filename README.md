# Sequelize Table Associations (Joins)



# Introduction
Sequelize is a Node package that allows the developer to interact with a variety of SQL databases using a single API. Specifically, it performs Object Relational Mapping (ORM) between your backend code and a SQL database.

This means you, the developer, can write object-oriented code and Sequelize will translate it into a SQL dialect. Whether you're using MySQL, SQLite, MSSQL, or PostgreSQL, Sequelize has you covered. Indicate a database dialect in your configuration file, and Sequelize takes care of the rest.

